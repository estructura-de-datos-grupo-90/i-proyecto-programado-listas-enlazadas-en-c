#include <stdio.h>
#include "lista_carta.h"
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>

lista_carta *ref_lista = NULL;

nodo_carta* crear_nodo(int nuevo_valor, int nuevo_valor_de_familia){

/*-----------------------------------------------------------------------
	crear_nodo: 
	Entradas: int nuevo_valor, int nuevo_valor_de_familia
	Salidas: nodo con los valores insertados
	Funcionamiento: crea un nodo de carta y le asigna su valor y valor de familia
-----------------------------------------------------------------------*/

	nodo_carta* nuevo;
	nuevo = malloc (sizeof (nodo_carta));
	nuevo->valor = nuevo_valor;
  nuevo->valor_de_familia = nuevo_valor_de_familia;
  switch(nuevo_valor){
    case 1:
    nuevo->nombre = "As";
    break;
    case 2:
    nuevo->nombre = "Dos";
    break;
    case 3:
    nuevo->nombre = "Tres";
    break;
    case 4:
    nuevo->nombre = "Cuatro";
    break;
    case 5:
    nuevo->nombre = "Cinco";
    break;
    case 6:
    nuevo->nombre = "Seis";
    break;
    case 7:
    nuevo->nombre = "Siete";
    break;
    case 8:
    nuevo->nombre = "Ocho";
    break;
    case 9:
    nuevo->nombre = "Nueve";
    break;
    case 10:
    nuevo->nombre = "Diez";
    break;
    case 11:
    nuevo->nombre = "Jester";
    break;
    case 12:
    nuevo->nombre = "Reina";
    break;
    case 13:
    nuevo->nombre = "Rey";
    break;
    default:
    printf("\nTu codigo exploto de manera ejemplar");
    break;
  }
  switch(nuevo_valor_de_familia){

    case 0:
    nuevo->nombre_de_familia = "Espadas";
    break;
    case 13:
    nuevo->nombre_de_familia = "Corazones";
    break;
    case 26:
    nuevo->nombre_de_familia = "Diamantes";
    break;
    case 39:
    nuevo->nombre_de_familia = "Tréboles";
    break;
    default:
    printf("\nTu codigo exploto de manera ejemplar");
    break;
  }
	nuevo->ref_siguiente = NULL;
  nuevo->ref_anterior = NULL;
	return nuevo;
}

void inicializar_lista(){

/*-----------------------------------------------------------------------
	inicializar_lista
	Entradas: -
	Salidas: -
	Funcionamiento: crea una lista doblemente enlazada vacia
-----------------------------------------------------------------------*/

	ref_lista = malloc(sizeof (lista_carta));
	ref_lista->cantidad=0;
	ref_lista->ref_inicio = NULL;
  ref_lista->ref_anterior = NULL;
}

void insertar_inicio(nodo_carta* nuevo){

/*-----------------------------------------------------------------------
	insertar_inicio
	Entradas: nuevo nodo de carta a insertar
	Salidas: -
	Funcionamiento: inserta un nuevo nodo de carta en la primera posicion de la baraja
-----------------------------------------------------------------------*/

	if (ref_lista == NULL){
		inicializar_lista();
	}
	nuevo->ref_siguiente = ref_lista->ref_inicio;
	ref_lista->ref_inicio = nuevo;
	ref_lista->cantidad = ref_lista->cantidad+1;
}

void insertar_final(nodo_carta* nuevo){

/*-----------------------------------------------------------------------
	insertar_final
	Entradas: nuevo nodo de carta a insertar
	Salidas: -
	Funcionamiento: inserta el nodo de carta en la ultima posicion de la baraja
-----------------------------------------------------------------------*/

	if (ref_lista == NULL){
	  inicializar_lista();
	}
	if (ref_lista->cantidad == 0){
	   insertar_inicio(nuevo);
	}else{
		nodo_carta* temporal;
  		temporal = malloc (sizeof (nodo_carta));
  		temporal = ref_lista->ref_inicio;
		while (temporal->ref_siguiente != NULL){
			temporal = temporal->ref_siguiente;
		}
		ref_lista->cantidad++;
    nuevo->ref_anterior = temporal;
    nuevo->ref_siguiente = NULL;
    temporal->ref_siguiente = nuevo;
 	}
}

void imprimirActual(nodo_carta* Actual, int cont){

/*-----------------------------------------------------------------------
	imprimirActual
	Entradas: puntero al nodo de carta, carta a imprimir, un contador
	Salidas: carta actual
	Funcionamiento: analiza el valor y valor de familia contenido en el nodo_carta, luego imprime una representacion visual de la carta correspondiente
-----------------------------------------------------------------------*/
    long int valor_carta = 0x1F0A1; //representación UNICODE: se usa en valor hexadecimal. se empieza por el as de espadas, y de ahí calcula los cambios

    setlocale(LC_CTYPE, "");


switch(Actual->valor){
    case 1:
    break;
    case 2:
    valor_carta = valor_carta + 0x1; //las cartas en UNICODE tienen valores adyacentes
    break;
    case 3:
    valor_carta = valor_carta + 0x2;
    break;
    case 4:
    valor_carta = valor_carta + 0x3;
    break;
    case 5:
    valor_carta = valor_carta + 0x4;
    break;
    case 6:
    valor_carta = valor_carta + 0x5;
    break;
    case 7:
    valor_carta = valor_carta + 0x6;
    break;
    case 8:
    valor_carta = valor_carta + 0x7;
    break;
    case 9:
    valor_carta = valor_carta + 0x8;
    break;
    case 10:
    valor_carta = valor_carta + 0x9;
    break;
    case 11:
    valor_carta = valor_carta + 0xA;
    break;
    case 12:
    valor_carta = valor_carta + 0xC; //se salta 1 valor ya que UNICODE tiene un símbolo de "Knight", una carta que se usa en algunas barajas y está entre el Jester y la Reina
    break;
    case 13:
    valor_carta = valor_carta + 0xD;
    break;
    default:
    printf("\nTu codigo exploto de manera ejemplar");
    break;
  }

    switch(Actual->valor_de_familia) //se realiza un switch para definir la familia y el color imprimido
    {
        case 0:
            printf("\n[%d]\033[22;30m  %lc  %s\x1b[0m de\033[22;30m %s\x1b[0m", cont, valor_carta, Actual->nombre, Actual->nombre_de_familia);
            break;
        case 13:
            valor_carta = valor_carta + 0x10; //en UNICODE, cartas de diferentes familias tienen diferente valor en el segundo dígito
            printf("\n[%d]\033[22;31m  %lc  %s\x1b[0m de\033[22;31m %s\x1b[0m", cont, valor_carta, Actual->nombre, Actual->nombre_de_familia);
            break;
        case 26:
            valor_carta = valor_carta + 0x20;
            printf("\n[%d]\033[22;31m  %lc  %s\x1b[0m de\033[22;31m %s\x1b[0m", cont, valor_carta, Actual->nombre, Actual->nombre_de_familia);
            break;
        case 39:
            valor_carta = valor_carta + 0x30;
            printf("\n[%d]\033[22;30m  %lc  %s\x1b[0m de\033[22;30m %s\x1b[0m", cont, valor_carta, Actual->nombre, Actual->nombre_de_familia);
            break;
        default:
            printf("algo está mal");
            break;
    }
}

void imprimir(){

/*-----------------------------------------------------------------------
	imprimir
	Entradas: baraja de cartas
	Salidas: Imprime la baraja entera, en orden de posición
	Funcionamiento: con un while, se imprimen todas las cartas, una a la vez, contando las 52 cartas imprimidas
-----------------------------------------------------------------------*/

	printf("\n---------------------------------------------------------------------");
	printf("\n Cartas");
	printf("\n---------------------------------------------------------------------");
	int cont = 0;
	nodo_carta* temporal;
  temporal = malloc (sizeof (nodo_carta));
  temporal = ref_lista->ref_inicio;
	while (cont < 52){
			imprimirActual(temporal, cont);
			temporal = temporal->ref_siguiente;
			cont++;
		}
}
void barajar(int o){
  o++;
  if(o == 500){
    return ;
  }
  int num = (rand()%51+1), x, a;
	nodo_carta* temporal1;
  nodo_carta* temporal2;
  nodo_carta* temp;
  temporal1 = malloc (sizeof (nodo_carta));
  temporal2 = malloc (sizeof (nodo_carta));
  temp = malloc (sizeof (nodo_carta));
  temporal1 = ref_lista->ref_inicio;
  temporal2 = temporal1;
  while(x!=num){
    if (temporal2->ref_siguiente == NULL){
      break;
     }
    else{
    temporal2 = temporal2->ref_siguiente;
    x++;
    }
   }
  temp->nombre = temporal1->nombre;
  temp->nombre_de_familia = temporal1->nombre_de_familia;
  temp->valor = temporal1->valor;
  temp->valor_de_familia = temporal1->valor_de_familia;
  temporal1->nombre = temporal2->nombre;
  temporal1->nombre_de_familia = temporal2->nombre_de_familia;
  temporal1->valor = temporal2->valor;
  temporal1->valor_de_familia = temporal2->valor_de_familia;
  temporal2->nombre = temp->nombre;
  temporal2->nombre_de_familia = temp->nombre_de_familia;
  temporal2->valor = temp->valor;
  temporal2->valor_de_familia = temp->valor_de_familia;
  num = 0;
 barajar(o);
}




struct nodo_carta *ultimo_nodo(nodo_carta *raiz){
		while (raiz->ref_siguiente != NULL){
			raiz = raiz->ref_siguiente;
		}
    return raiz;
}

nodo_carta* partition(nodo_carta *l, nodo_carta *h) 
{ 

    int x  = h->valor + h->valor_de_familia; 
  
    nodo_carta* temp;
    temp = malloc (sizeof (nodo_carta));


    nodo_carta *i = l->ref_anterior; 
  

    for (nodo_carta *j = l; j != h; j = j->ref_siguiente) 
    { 
        if ( (j->valor + j->valor_de_familia) <= x) 
        { 
            i = (i == NULL)? l : i->ref_siguiente; 
  
            temp->nombre = i->nombre;
            temp->nombre_de_familia = i->nombre_de_familia;
            temp->valor = i->valor;
            temp->valor_de_familia = i->valor_de_familia;
            i->nombre = j->nombre;
            i->nombre_de_familia = j->nombre_de_familia;
            i->valor = j->valor;
            i->valor_de_familia = j->valor_de_familia;
            j->nombre = temp->nombre;
            j->nombre_de_familia = temp->nombre_de_familia;
            j->valor = temp->valor;
            j->valor_de_familia = temp->valor_de_familia;

        } 
    } 
    i = (i == NULL)? l : i->ref_siguiente; // Similar to i++ 
            temp->nombre = i->nombre;
            temp->nombre_de_familia = i->nombre_de_familia;
            temp->valor = i->valor;
            temp->valor_de_familia = i->valor_de_familia;
            i->nombre = h->nombre;
            i->nombre_de_familia = h->nombre_de_familia;
            i->valor = h->valor;
            i->valor_de_familia = h->valor_de_familia;
            h->nombre = temp->nombre;
            h->nombre_de_familia = temp->nombre_de_familia;
            h->valor = temp->valor;
            h->valor_de_familia = temp->valor_de_familia;
    return i; 
} 

void _quickSort(struct nodo_carta* l, struct nodo_carta* h){
  if (h != NULL && l != h && l != h->ref_siguiente) 
    { 
        struct nodo_carta *p = partition(l, h); 
        _quickSort(l, p->ref_anterior); 
        _quickSort(p->ref_siguiente, h); 
    } 
}

void quickSort(struct nodo_carta* head){

  struct nodo_carta *h = ultimo_nodo(head);
  _quickSort(head, h); 

		}


void menu() {

/*-----------------------------------------------------------------------
	menu
	Entradas: ninguna
	Salidas: -
	Funcionamiento: luego de crearse la baraja, se realiza un ciclo infinito para que el usuario escoja la cantidad de opciones que quiera, en cualquier orden, hasta que desee terminar.
-----------------------------------------------------------------------*/

	size_t max_size;
    int opcion,cont, a;
    cont = 0;
    a = 0;
    nodo_carta* temporal;
    for(int x = 0; x <= 39; x = x + 13){
        for(int y = 1; y <= 13; y++){
          temporal = crear_nodo(y, x);
    			insertar_final(temporal);
        }
      }
    nodo_carta* Actual;
    Actual = ref_lista->ref_inicio;
	do{
		printf("\n\n\n\n\n");
		printf("\n---------------------------------------------------------------------");
		printf("\n(0) Salir");
		printf("\n(1) Para mostrar todas las cartas");
    printf("\n(2) Para mostrar la carta actual");
		printf("\n(3) Para mostrar la siguiente carta a la actual");
		printf("\n(4) Para mostrar la carta anterior a la actual");
    printf("\n(5) Para barajar las cartas");
    printf("\n(6) Para ordenar las cartas de regreso al orden original");
    printf("\n(7) Salir");
		printf("\n---------------------------------------------------------------------");
		printf("\n>> Opcion deseada: ");
		max_size = 1;
		opcion = get_user_numerical_input(max_size);
		switch(opcion){
			case 0: 
				break;
			case 1: 
        imprimir();
				break;
      case 2:
        imprimirActual(Actual, cont);
        break;
			case 3:
      if(Actual->ref_siguiente == NULL){
        	printf("\nHa llegado al final de la baraja");
      }else{Actual = Actual->ref_siguiente;
      cont = cont + 1;
      imprimirActual(Actual, cont);
      }
				break;
      case 4:
      if(Actual->ref_anterior == NULL){
        	printf("\nHa llegado al inicio de la baraja");
      }else{Actual = Actual->ref_anterior;
      cont = cont - 1;
      imprimirActual(Actual, cont);
      }
				break;
      case 5:
       barajar(a);
       break;
      
      case 6:
        quickSort(ref_lista->ref_inicio);
        break;
      case 7:
      printf("\n Gracias por jugar \n");
      exit(0);
		}
	}while(opcion != 0);
}

int main() {
	menu();
	return 0;
}

char* get_user_input(size_t max_size){
    
    char *buffer;
    size_t characters;

    buffer = (char *)malloc(max_size * sizeof(char));
    if(buffer == NULL){
        perror("ERROR No fue posible reservar memoria para el buffer");
        exit(1);
    }
    characters = getline(&buffer,&max_size,stdin);
    buffer[strlen(buffer)-1]= 0;
    return buffer;
}

int get_user_numerical_input(size_t max_size){
	int numerical_input = atoi(get_user_input(max_size));
	return numerical_input;
}