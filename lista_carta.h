//Definición de Estructuras
typedef struct nodo_carta
{
	int valor, valor_de_familia;
	char *nombre, *nombre_de_familia;

	struct nodo_carta *ref_siguiente;
  struct nodo_carta *ref_anterior;
}nodo_carta;

typedef struct lista_carta
{
	nodo_carta *ref_inicio;
  nodo_carta *ref_anterior;
	int cantidad;
}lista_carta;

//Definición de Funciones
/*-----------------------------------------------------------------------
	crear_nodo
	Entradas: No recibe parámetros
	Salidas: Retorna un puntero de tipo nodo_estudiante al nodo creado
	Funcionamiento: 
		- Solicita al usuario ingresar Nombre y Carnet.
		- Crea un puntero de tipo nodo_estudiante
		- Le asigna al nodo el nombre y carnet ingresados.
		- El nodo apunta a NULL.
		- retorna el puntero al nuevo nodo.
-----------------------------------------------------------------------*/
nodo_carta* crear_nodo();
/*-----------------------------------------------------------------------
	inicializar_lista
	Entradas: 
	Salidas: 
	Funcionamiento: 
-----------------------------------------------------------------------*/
void inicializar_lista();
/*-----------------------------------------------------------------------
	insertar_inicio
	Entradas: 
	Salidas: 
	Funcionamiento: 
-----------------------------------------------------------------------*/
void insertar_final(nodo_carta* nuevo);
/*-----------------------------------------------------------------------
	borrar_por_indice
	Entradas: 
	Salidas: 
	Funcionamiento: 
-----------------------------------------------------------------------*/

void menu();
 /*-----------------------------------------------------------------------
	main
	Entradas: 
	Salidas: 
	Funcionamiento: 
-----------------------------------------------------------------------*/
int main();
 /*-----------------------------------------------------------------------
	get_user_input
	Entradas: 
	Salidas: 
	Funcionamiento: 
-----------------------------------------------------------------------*/
char* get_user_input(size_t max_size);
 /*-----------------------------------------------------------------------
	get_user_numerical_input
	Entradas: 
	Salidas: 
	Funcionamiento: 
-----------------------------------------------------------------------*/
int get_user_numerical_input(size_t max_size);
